/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.manprod.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ciisa.manprod.model.dao.ProductoDAO;
import ciisa.manprod.model.dao.exceptions.NonexistentEntityException;
import ciisa.manprod.model.entities.Producto;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author javi3
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    ProductoDAO dao = new ProductoDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");
        if (accion.equalsIgnoreCase("Crear")) {
            System.out.println("crear");
            request.getRequestDispatcher("agregar.jsp").forward(request, response);   
        } else if (accion.equalsIgnoreCase("Guardar")) {
            System.out.println("guardar");
            String nom = request.getParameter("nombre");
            String com = request.getParameter("complemento");
            String uni = request.getParameter("unidad");
            String sto = request.getParameter("stock");

            Producto prd = new Producto();

            prd.setNombre(nom);
            prd.setComplemento(com);
            prd.setUnidad(uni);
            prd.setStock(Long.parseLong(sto));

            try {
                dao.create(prd);
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        } else if (accion.equalsIgnoreCase("Listar")) {
            System.out.println("listar");
            List<Producto> productos = dao.findProductoEntities();
            request.setAttribute("productos", productos);
            request.getRequestDispatcher("index.jsp?accion=Listar").forward(request, response);
        } else if (accion.equalsIgnoreCase("Actualizar")) {
            Long id = Long.parseLong(request.getParameter("id"));
            Producto prd = dao.findProducto(id);
            request.setAttribute("producto", prd);
            request.getRequestDispatcher("editar.jsp").forward(request, response);

            // Graba los cambios en el registro
        } else if (accion.equalsIgnoreCase("Grabar")) {
            Producto prd = new Producto();

            prd.setId(Long.parseLong(request.getParameter("txtId")));
            prd.setNombre(request.getParameter("txtNombre"));
            prd.setComplemento(request.getParameter("txtComplemento"));
            prd.setUnidad(request.getParameter("txtUnidad"));
            prd.setStock(Long.parseLong(request.getParameter("txtStock")));

            EntityManagerFactory emf;
            emf = Persistence.createEntityManagerFactory("dbproducts");
            ProductoDAO con = new ProductoDAO(emf);
            
            try {
                con.edit(prd);
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        }else if (accion.equalsIgnoreCase("Eliminar")) {
            Long id = Long.parseLong(request.getParameter("id"));
            try {
                dao.destroy(id);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        }else if (accion.equalsIgnoreCase("Cancelar")) {
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
