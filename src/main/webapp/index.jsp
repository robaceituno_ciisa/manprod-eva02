<%-- 
    Document   : index
    Created on : 26-04-2020, 21:41:44
    Author     : mono
--%>

<%@page import="ciisa.manprod.model.dao.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="ciisa.manprod.model.entities.Producto"%> 
<%@page import="ciisa.manprod.controller.Controller"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css"/>
        <title>Mantenedor de Productos</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">

            <div class="mx-auto order-0">
                <a class="navbar-brand mx-auto" href="#"><h1>Mantenedor de Productos</h1></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <br>
        <div class="container-fluid h-100">
            <form action="controller" method="POST">
                <input type="submit" value="Crear" name="accion" />
                <input type="submit" value="Listar" name="accion" /><br>
            </form>
            <table border="1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Complemento</th>
                        <th>Unidad</th>
                        <th>Stock</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="producto" items="${productos}">
                        <tr>
                            <th>${producto.getId()}</th>
                            <th>${producto.getNombre()}</th>
                            <th>${producto.getComplemento()}</th>
                            <th>${producto.getUnidad()}</th>
                            <th>${producto.getStock()}</th>
                            <th>
                                <form action="controller" method="POST">
                                    <input type="hidden" name="id" value="${producto.getId()}" />
                                    <input type="submit" name="accion" value="Actualizar" />
                                    <input type="submit" name="accion" value="Eliminar" />
                                </form>
                            </th>
                        </tr>
                    </c:forEach>
                </tbody>

            </table>

        </div>
        <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Taller Aplicaciones Empresariales - UNI02 - CIISA 2020 ::: Robinson Aceituno</span>
            </div>
        </footer>
    </body>
</html>
