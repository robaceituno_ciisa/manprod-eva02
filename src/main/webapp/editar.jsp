<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ciisa.manprod.model.entities.Producto" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Producto</title>
    </head>
    <body>
        <h1>Edicion de Productos</h1>
        <form action="controller" method="POST">
            ID:
            <input type="text" name="txtId" value="${producto.getId()}" readonly/><br><br>
            Nombre:
            <input type="text" name="txtNombre" value="${producto.getNombre()}" /><br><br>
            Complemento:
            <input type="text" name="txtComplemento" value="${producto.getComplemento()}" /><br><br>
            Unidad:
            <input type="text" name="txtUnidad" value="${producto.getUnidad()}" /><br><br>
            Stock:
            <input type="number" name="txtStock" value="${producto.getStock()}" /><br><br>
            <input type="submit" value="Grabar" name="accion" />
        </form>
    </body>
</html>